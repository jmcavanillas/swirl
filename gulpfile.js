var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
// var sourcemaps = require('gulp-sourcemaps');
// var autoprefixer = require('gulp-autoprefixer');

var sass_devel_opt = {
    errLogToConsole: true,
    outputStyle: 'Expanded'
};

var paths = 
{
    'fontawesome': './node_modules/@fortawesome/fontawesome-free/webfonts/*',
    'fontawesome-sass': './static/scss/fontawesome.scss',
    'source-sass': './static/scss/style.scss',
    'transpiled-css': './static/css/',
    'fonts': './static/fonts/'
};

gulp.task('sass', function() {
    return gulp
        .src(paths['source-sass'])
        // .pipe(sourcemaps.init())
        .pipe(sass(sass_devel_opt).on('error', sass.logError))
        // .pipe(sourcemaps.write())
        // .pipe(autoprefixer())
        // .pipe(gulp.dest(sass_output)
        .pipe(concat('all.min.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest(paths['transpiled-css']))
});

gulp.task('fontawesome-sass', function() {
    return gulp
        .src(paths['fontawesome-sass'])
        // .pipe(sourcemaps.init())
        .pipe(sass(sass_devel_opt).on('error', sass.logError))
        // .pipe(sourcemaps.write())
        // .pipe(autoprefixer())
        // .pipe(gulp.dest(sass_output)
        .pipe(concat('fontawesome.min.css'))
        //.pipe(cleanCSS())
        .pipe(gulp.dest(paths['transpiled-css']))
});

gulp.task('sass:watch', function() {
    return gulp
    .watch([paths['source-sass']], gulp.series('sass'))
        .on('change', function(event) {
            console.log('File' + event.path + ' was ' + event.type + ', running tasks...');
        });
});

gulp.task('fontawesome', () => gulp.src(paths['fontawesome']).pipe(gulp.dest(paths['fonts'])))
gulp.task('fontawesome-css', () => gulp.src(paths['fontawesome-css']).pipe(gulp.dest(paths["transpiled-css"] + '/fontawesome/')))

gulp.task('fonts', gulp.series('fontawesome', 'fontawesome-sass'));

gulp.task('prod', function () {
  return gulp
    .src([paths['source-sass']])
    .pipe(sass({ outputStyle: 'compressed' }))
    .pipe(autoprefixer())
    .pipe(gulp.dest(sass_output));
});

gulp.task('default', gulp.parallel('sass', 'sass:watch'));
