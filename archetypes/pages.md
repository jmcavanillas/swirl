---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
layout: "page"
toc: false
sidebar_hidden: true
---