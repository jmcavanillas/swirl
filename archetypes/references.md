---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
link: https://developer.nvidia.com/gpugems/GPUGems3/gpugems3_ch07.html
---
